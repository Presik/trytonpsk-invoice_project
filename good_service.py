# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class GoodServiceSupplier(ModelSQL, ModelView):
    "Good Service Supplied"
    __name__ = 'account.invoice.good_service_supplied'
    name = fields.Char('Name', required=True)

    @classmethod
    def __setup__(cls):
        super(GoodServiceSupplier, cls).__setup__()
        cls._order.insert(0, ('id', 'DESC'))

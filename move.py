# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.i18n import gettext
# from .exceptions import AnalyticError
from trytond.model import fields
from trytond.transaction import Transaction


class MoveLine(metaclass=PoolMeta):
    __name__ = 'account.move.line'

    project = fields.Function(fields.Many2One('project.work', 'Project'),
        'get_project')

    # @property
    # def rule_pattern_warehouse(self):
    #     return {
    #         'warehouse': self.move.origin.to_location.id if hasattr(self.move.origin, 'to_location') \
    #             and self.move.origin.to_location else None
    #         }

    # @classmethod
    # def apply_rule(cls, lines):
    #     pool = Pool()
    #     Rule = pool.get('analytic_account.rule')

    #     rules = Rule.search([])

    #     for line in lines:
    #         if line.analytic_lines:
    #             continue
    #         pattern = line.rule_pattern
    #         if hasattr(line, 'rule_pattern_warehouse'):
    #             pattern_warehouse = line.rule_pattern_warehouse
    #             pattern.update(pattern_warehouse)
    #         for rule in rules:
    #             if rule.match(pattern):
    #                 break
    #         else:
    #             continue
    #         analytic_lines = []
    #         for entry in rule.analytic_accounts:
    #             analytic_lines.extend(
    #                 entry.get_analytic_lines(line, line.move.post_date))
    #         line.analytic_lines = analytic_lines
    #     super(MoveLine, cls).apply_rule(lines)

    def get_project(self, name):
        if self.move.origin and self.move.origin.__name__ == 'account.invoice' and self.move.origin.project:
            return self.move.origin.project.id


# class Move(metaclass=PoolMeta):
#     __name__ = 'account.move'

#     @classmethod
#     def validate_move(cls, moves):
#         super(Move, cls).validate_move(moves)
#         for move in moves:
#             if move.state != 'draft' and move.period.type != 'adjustment':
#                 for line in move.lines:
#                     if line.account and line.account.type.statement != 'balance' and \
#                         not line.analytic_lines and not line.account.code.startswith('4'):
#                         raise AnalyticError(
#                             gettext('analytic_co.msg_analytic_error',
#                             line=line.rec_name, account=line.account.rec_name)
#                         )
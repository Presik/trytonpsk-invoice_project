# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import invoice
from . import work
from . import purchase
from . import good_service
from . import move


def register():
    Pool.register(
        purchase.Purchase,
        invoice.Invoice,
        invoice.AddAnalyticAccountStart,
        work.Work,
        invoice.InvoiceAddProjectStart,
        invoice.InvoiceTaxesByProjectStart,
        purchase.AddPurchaseAnalyticAccountStart,
        invoice.InvoicesProjectPurchaseStart,
        good_service.GoodServiceSupplier,
        invoice.PortfolioDetailedStart,
        purchase.PurchaseRequisition,
        purchase.PurchaseRequest,
        purchase.PurchaseRequisitionLine,
        move.MoveLine,
        module='invoice_project', type_='model')
    Pool.register(
        invoice.InvoiceAddProject,
        invoice.InvoiceTaxesByProject,
        purchase.AddPurchaseAnalyticAccount,
        invoice.InvoicesProjectPurchase,
        invoice.PortfolioDetailed,
        module='invoice_project', type_='wizard')
    Pool.register(
        invoice.InvoiceTaxesByProjectReport,
        invoice.InvoicesProjectPurchaseReport,
        invoice.PortfolioDetailedReport,
        module='invoice_project', type_='report')
